# what to do with carpositions with no gameTick?
# record max angle etc. and adjust

### ## ## ## ## ##
SETTINGS
### ## ## ## ## ###

debug = true

### ## ## ## ## ##
DEPENDENCIES
### ## ## ## ## ###

fs = require "fs"

openLog =  ->
  fileName = "data_#{race.gameId}.csv"
  fs.writeFile "data.cvs", "data \n", (error) ->
    console.error("Error writing file", error) if error

writeLog = (isFirstLine, line) ->
  try
    fileName = "data_#{race.gameId}.csv"
    if isFirstLine 
      data =  "server: " + serverHost + " carLength: " + myCar.length + " " + " carWidth: " + myCar.width + " flagPosition: " + myCar.flagPosition + 
      " " + "'frictionAcceleration'" + getFrictionAcceleration() + " \n" + 
      "gametick drag enginePower throttle speed acceleration centripetalAcc pieceIndex pieceLength carAngle inPieceDistance radius pieceAngle laneLength"
      console.log data
    else
      radius = "-"
      angle = "-"
      length = "-"
      laneLength = "-"
      if isCurve (state.position.piecePosition.pieceIndex)
        radius = race.track.pieces[state.position.piecePosition.pieceIndex].radius
        angle = race.track.pieces[state.position.piecePosition.pieceIndex].angle
        length = arcLength radius, angle
        laneLength = arcPieceLaneLength(race.track.pieces[state.position.piecePosition.pieceIndex], state.position.piecePosition.lane.startLaneIndex)
      else
        length = race.track.pieces[state.position.piecePosition.pieceIndex].length
      data = state.gameTick + " " + phys.drag + " "  + phys.enginePower + " " + state.lastThrottle + " " + state.speed  + " " + state.acceleration +
      " " + state.centripetalAcceleration + " " + state.position.piecePosition.pieceIndex + " " + length +
      " " + state.position.angle + " " + state.position.piecePosition.inPieceDistance + " " + radius + " " +
      angle + " " + laneLength 
    console.log "writeLogData: " + data # for competitions
  catch error
    console.log "error writing log file"
  fs.appendFile fileName, data + "\n", (error) ->
    console.error("Error writing file", error) if error

### ## ## ## ## ##
NETWORKING
### ## ## ## ## ###

net        = require("net")
JSONStream = require('JSONStream')

serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort)

client = net.connect serverPort, serverHost, () ->
  send({ msgType: "join", data: { name: botName, key: botKey }})
#  send({ msgType: "createRace", data: { botId: { name: botName, key: botKey }, trackName: "imola", password: "jeaaaa", carCount: 1}})

send = (json) ->
  client.write JSON.stringify(json)
  if debug then console.log "<- SEND: " + JSON.stringify(json)
  client.write '\n'
 
jsonStream = client.pipe(JSONStream.parse())

jsonStream.on 'data', (data) ->
  if debug then console.log "-> RECEIVE:" + JSON.stringify data
  switch data.msgType
    when 'gameStart'
      console.log 'Race started'        
      state.gameTick = 0
      send {msgType: "throttle", data: 1.0} # obligatory response
    when 'carPositions'
     if data.gameTick?  # respond to carPositions that have gameTick specified
        if data.gameTick == 1
          console.log "here"
          race.gameId = data.gameId
          writeLog(true) 
        if race.competition then sendNextMove(data)
        else #qualification and test races
          if data.gameTick < 6 
              send gatherStartPhysicsData data.data, data.gameTick
          else if mode.frictionUnknown
              send driveIntoFirstCornerFast data.data, data.gameTick
          else 
            sendNextMove(data)
      else
        gatherStartPhysicsData data.data, 0
    when 'join'
      console.log 'Joined'
    when 'yourCar'
      setUpCarInfo data      
    when 'gameInit'
      console.log 'Game init'
      if data.data.race.raceSession.quickRace?
        if not data.data.race.raceSession.quickRace
          console.log "RACE TYPE: COMPETITION"
          race.competition = true
        else 
          console.log "RACE TYPE: TEST"
          race.competition = false
      else 
        console.log "RACE TYPE: TEST OR QUALIFICATION"
        race.competition = false
      setUpGame data
    when 'gameEnd'
      console.log 'Race ended'

sendNextMove = (data) ->
  updateSituation data.data, data.gameTick
  writeLog()
  move = nextMove()
  if debug then console.log "move type:" + move.type 
  switch move.type
    when 'brake'
      send getThrottleMsg 0, state.gameTick
    when 'throttle'
      send getThrottleMsg move.amount, state.gameTick
    when 'switchLane'
      state.lastThrottle = "-"
      send getSwitchLaneMsg move.amount, state.gameTick
  
jsonStream.on 'error', ->
  if debug then console.log "disconnected"

### ## ## ## ## ##
GAME STATE
### ## ## ## ## ###

myCar = {}
myCar.name = null
myCar.color = null  
myCar.length = null
myCar.width = null
myCar.flagPosition = null

state = {}
state.position = null
state.gameTick = null
state.gameTickDelta = 1
state.acceleration = null
state.lastSpeed = null
state.speed = null
state.lastThrottle = 0
state.centripetalAcceleration = null
state.lap = null

mode = {}
mode.switchLanePending = false
mode.switchLanePendingDirection = 0
mode.frictionUnknown = true

phys = {}
phys.force = 0
DEFAULT_FRICTION = 0.032
phys.friction = DEFAULT_FRICTION  # acting in angles = v^2/rg irtoamishetkellä
phys.drag = 0.98
phys.enginePower = 1
phys.enginePowerMinusDrag = 1
phys.g = 9.81 #changing??
phys.curveTestSpeed = 5.0

race = {}
race.gameId = "def"
race.type = null
race.track = null
race.trackNumPieces = 0
race.lanes = null
race.competition = true


setUpGame = (data) ->
  race.track = data.data.race.track
  race.trackNumPieces = race.track.pieces.length
  race.lanes = race.track.lanes

  carInfo = getCarInfo(data.data.race.cars, myCar.color, myCar.name)
  myCar.length = carInfo.dimensions.length
  myCar.width = carInfo.dimensions.width
  myCar.flagPosition = carInfo.dimensions.guideFlagPosition

  firstCurveIndex = getNextCurve 0
  firstCurveRadius = race.track.pieces[firstCurveIndex].radius

  if firstCurveRadius < 60
    phys.curveTestSpeed = 4.5
  else if firstCurveRadius < 110
    phys.curveTestSpeed = 6.0
  else if firstCurveRadius < 160
    phys.curveTestSpeed = 7.5
  else if firstCurveRadius < 210
    phys.curveTestSpeed = 9

  if debug then console.log "Track length : " + race.trackNumPieces  

setUpCarInfo = (data) ->
  myCar.name = data.data.name
  myCar.color = data.data.color
  if debug then console.log "Car info OK :" + JSON.stringify(myCar)

setGameTick = (newGameTick) ->
  state.gameTickDelta = newGameTick -  state.gameTick
  state.gameTick = newGameTick  
  
# MATH
arcPieceLaneLength = (piece, laneIndex) ->
  arcLength(getLaneRadius(piece, laneIndex), piece.angle)
  
arcLength = (radius, angle) ->
  2*Math.PI*radius*Math.abs(angle)/360

acceleration = (lastSpeed, curSpeed) ->
  curSpeed - lastSpeed

getCentripetalAcceleration = (speed, radius) ->
  (speed * speed) / radius
  
getFrictionAcceleration =  ->
  phys.friction * phys.g

getSpeedForThrottle = (throttle) ->
  (phys.enginePower / phys.drag) / throttle

getThrottleForSpeed = (speed) ->
  throttle = (speed * (1 - phys.drag)) / phys.enginePowerMinusDrag
  if throttle > 1.0
    1.0
  else if throttle < 0.0
    0.0
  else
    throttle

#hasEnoughStraigthsForTurbo = (index) ->
#  lengthOfStraight = 0
#  nextPieceIndex = getNextPieceIndex(index)
#  while not isCurve ()

getNextPieceIndex = (index) ->
  nextPieceIndex = 0
  if index == race.trackNumPieces - 1
      nextPieceIndex = 0
  else
    nextPieceIndex = index + 1
  nextPieceIndex

getLaneRadius = (piece, laneIndex) ->
  distance = race.lanes[laneIndex].distanceFromCenter # negative if left from center
  radius = 0
  if piece.angle > 0 # right turn
    radius = piece.radius - distance
  else
    radius = piece.radius + distance
  radius


getMaxSpeed = (radius) ->
  Math.sqrt(phys.friction*radius*phys.g)

calculateSpeed = (position, lastPosition, gameTickDelta) ->
  speed = 0
  #TODO don't expect to receive all ticks..
  if position.piecePosition.pieceIndex is lastPosition.piecePosition.pieceIndex
    speed = position.piecePosition.inPieceDistance - lastPosition.piecePosition.inPieceDistance
  else # car has driven to the next piece 
    if position.piecePosition.pieceIndex - 1 is lastPosition.piecePosition.pieceIndex
      console.log "entering"
      lastPiece = race.track.pieces[lastPosition.piecePosition.pieceIndex]
      pieceLength = lastPiece.length ? arcPieceLaneLength(lastPiece, lastPosition.piecePosition.lane.startLaneIndex) 
      speed = pieceLength - lastPosition.piecePosition.inPieceDistance + position.piecePosition.inPieceDistance
      console.log pieceLength, lastPosition.piecePosition.inPieceDistance, position.piecePosition.inPieceDistance
      console.log "speed on " + speed
    else 
      console.log "Speed calculation error: missing information"
  speed

getFollowingPieceIndex = (pieceIndex) ->
  if (pieceIndex is race.track.pieces.length - 1)
    return 0
  else
    pieceIndex + 1

getDistanceTo = (position, targetPieceIndex, targetInPieceDistance) ->
  curPieceIndex = position.piecePosition.pieceIndex
  curPiece = race.track.pieces[curPieceIndex]
  curPieceLength = if isCurve (curPieceIndex) then arcPieceLaneLength(curPiece, position.piecePosition.lane.startLaneIndex) else curPiece.length
  distance = curPieceLength - position.piecePosition.inPieceDistance # to go in this piece

  if curPieceIndex == race.trackNumPieces - 1
      curPieceIndex = 0
  else
    curPieceIndex++
  while curPieceIndex != targetPieceIndex
    distance +=race.track.pieces[curPieceIndex].length
    if curPieceIndex == race.trackNumPieces - 1
      curPieceIndex = 0
    else 
      curPieceIndex++
  distance += targetInPieceDistance

isCurve = (pieceIndex) ->
  if race.track.pieces[pieceIndex].length?
    return false
  else
    return true

getNextCurve = (pieceIndex) ->
  curPieceIndex = pieceIndex
  counter = race.trackNumPieces
  while counter > 0
    if curPieceIndex == race.trackNumPieces - 1
      curPieceIndex = 0
    else 
      curPieceIndex++
    if isCurve curPieceIndex
      return curPieceIndex
    else
      counter--

getCurveRadius = (pieceIndex) ->
  if (isCurve pieceIndex)
    return race.track.pieces[pieceIndex].radius
  else
    -1

getTimeToDecelerate = (curSpeed, targetSpeed) ->
  logn(targetSpeed / curSpeed, phys.drag) # decelerates ~2% per gametick

getBrakingDistance = (curSpeed, targetSpeed) ->
  true

logn = (x, base) ->
  return Math.log(x) / Math.log(base) 

gatherStartPhysicsData = (carPositions, gameTick) ->
  if debug then console.log "Gathering Physics Data" 

  setGameTick gameTick
  state.lastPosition = state.position
  state.position = getCarPositionData carPositions, myCar.color, myCar.name

  # tick zero, throttle 1.0
  # tick one, we'll see the "engine power - drag force" from the distance traveled, send throttle 0
  # tick two, we'll get the drag force, and can calculate the real engine power as well.

  switch gameTick
    when 0
       # at tick zero, we send throttle 1.0 to server
      state.lastPosition = state.position
      if debug then console.log "logged first position " + state.lastPosition
      getThrottleMsg 1, gameTick     
    when 1
      curSpeed = state.position.piecePosition.inPieceDistance - state.lastPosition.piecePosition.inPieceDistance # TODO: not robust... does not take into account changing pieces in between
      phys.enginePowerMinusDrag = curSpeed
      state.speed = curSpeed
      if debug then console.log "EnginePower " + phys.enginePowerMinusDrag 
      if debug then console.log "state.speed: " + state.speed
      writeLog() 
      getThrottleMsg 0, gameTick
    when 2
      state.lastSpeed = state.speed
      state.speed = calculateSpeed state.position, state.lastPosition, state.gameTickDelta
      state.acceleration = (state.speed - state.lastSpeed) / state.gameTickDelta    
      if debug then console.log "acceleration: " + state.acceleration 
      if debug then console.log "speed: " + state.speed      
      writeLog()
      getThrottleMsg 0, gameTick
    when 3
      state.lastSpeed = state.speed
      state.speed = calculateSpeed state.position, state.lastPosition, state.gameTickDelta
      if state.lastSpeed? and state.lastSpeed isnt 0
        phys.drag = state.speed / state.lastSpeed
        phys.enginePower = phys.enginePowerMinusDrag / phys.drag
      else
        console.log "ERROR in physics calculations!"
      if debug then console.log "acceleration: " + state.acceleration 
      if debug then console.log "speed: " + state.speed   
      if debug then console.log "drag: " + phys.drag
      writeLog()
      getThrottleMsg 0, gameTick
    when 4
      state.lastSpeed = state.speed
      state.speed = calculateSpeed state.position, state.lastPosition, state.gameTickDelta
      state.acceleration = (state.speed  - state.lastSpeed) / state.gameTickDelta
      if debug then console.log "acceleration: " + state.acceleration 
      if debug then console.log "speed: " + state.speed     
      writeLog()  
      getThrottleMsg 0, gameTick
    when 5
      state.lastSpeed = state.speed
      state.speed = calculateSpeed state.position, state.lastPosition, state.gameTickDelta
      state.acceleration = (state.speed  - state.lastSpeed) / state.gameTickDelta
      if debug then console.log "acceleration: " + state.acceleration 
      if debug then console.log "speed: " + state.speed
      writeLog()       
      getThrottleMsg 0, gameTick

# for the friction calculation to work, the car should just barely slip

# if the set speed doesn't work for the first corner we should try it on the next, and the next
# and if we already went the whole lap, then we should go a bit faster
driveIntoFirstCornerFast = (carPositions, gameTick) ->
  if debug then console.log "Gathering Cornering Data"
  lastSpeed = state.speed
  console.log "lastSpeed" + lastSpeed
  updateSituation carPositions, gameTick
  writeLog()

  if isCurve (state.position.piecePosition.pieceIndex)
    if state.position.angle isnt 0
      radius = getLaneRadius(race.track.pieces[state.position.piecePosition.pieceIndex], laneIndex = state.position.piecePosition.lane.startLaneIndex)
      console.log "lane radius: " + radius      
      newFriction = (lastSpeed * lastSpeed) / (radius * phys.g)
      if (newFriction < 0.042) # typically 0.032-0.036
        console.log "Friction set to :" + newFriction
        phys.friction = newFriction
        mode.frictionUnknown = false      
  if state.speed < (phys.curveTestSpeed - 0.01)
    throttle = 1.0
  else 
    throttle = getThrottleForSpeed(phys.curveTestSpeed)  
  getThrottleMsg throttle, state.gameTick


getThrottleMsg = (amount, gameTick) ->
  state.lastThrottle = amount
  {msgType: "throttle", data: amount, gameTick: gameTick}

getSwitchLaneMsg = (direction, gameTick) ->
  {msgType: "switchLane", data: direction, gameTick: gameTick}


getCarInfo = (cars, carColor, carName) ->
  for car in cars
    if car.id.name is carName and car.id.color is carColor
      return car

getCarPositionData = (carPositions, carColor, carName) ->
  for carPositionData in carPositions
    if carPositionData.id.name is carName and carPositionData.id.color is carColor
      return carPositionData

updateSituation = (carPositions, gameTick) ->
  setGameTick gameTick
  state.lastPosition = state.position
  state.position = getCarPositionData carPositions, myCar.color, myCar.name
  
  if state.lastPosition?  
    # speed and acceleration
    state.lastSpeed = state.speed
    state.speed = calculateSpeed state.position, state.lastPosition, state.gameTickDelta
    state.acceleration = (state.speed - state.lastSpeed) / state.gameTickDelta

    if debug then console.log "acceleration: " + state.acceleration 
    if debug then console.log "speed: " + state.speed
    # calculate centripetal acceleration
    if isCurve(state.position.piecePosition.pieceIndex)
      turnRadius = 1
      laneIndex = state.position.piecePosition.lane.startLaneIndex
      curvePiece = race.track.pieces[state.position.piecePosition.pieceIndex]
      if curvePiece.angle > 0 # right turn
        turnRadius = curvePiece.radius - race.lanes[laneIndex].distanceFromCenter  #negative distance from center means left to the center (adding to radius)
      else  # turn left
        turnRadius = curvePiece.radius + race.lanes[laneIndex].distanceFromCenter    
      state.centripetalAcceleration = getCentripetalAcceleration state.speed, turnRadius
      console.log "radius: " + race.track.pieces[state.position.piecePosition.pieceIndex].radius + " , angle:" + state.position.angle
    else
      state.centripetalAcceleration = 0
    console.log "centripetalAcceleration: " + state.centripetalAcceleration
    console.log "next curve" + getNextCurve(state.position.piecePosition.pieceIndex)
    if state.position.piecePosition.lane.startLaneIndex isnt state.position.piecePosition.lane.endLaneIndex
      mode.switchLanePending = false
      mode.switchLanePendingDirection = 0

hasToBrake = (position) ->
  #getNextCurve
  nextCurveIndex = getNextCurve position.piecePosition.pieceIndex
  #getMaxSpeedAtNextCurve
  radius = 100
  if mode.switchLanePending
    if mode.switchLanePendingDirection is "Left"
      radius = getLaneRadius(race.track.pieces[nextCurveIndex], state.position.piecePosition.lane.endLaneIndex - 1) 
    else 
      radius = getLaneRadius(race.track.pieces[nextCurveIndex], state.position.piecePosition.lane.endLaneIndex + 1) 
  else 
    radius = getLaneRadius(race.track.pieces[nextCurveIndex], state.position.piecePosition.lane.endLaneIndex) 

  targetSpeed = getMaxSpeed radius 
  if state.position.angle < 10 
    if nextCurveIndex is race.track.pieces.length - 1
        followingPieceIndex = 0
      else
        followingPieceIndex = nextCurveIndex + 1
      if followingPieceIndex is race.track.pieces.length - 1
        subsequentPieceIndex = 0
      else
        subsequentPieceIndex = followingPieceIndex + 1

      if not isCurve(followingPieceIndex) and not isCurve(subsequentPieceIndex)
        targetSpeed += 1.5
      else if not isCurve(subsequentPieceIndex) 
        targetSpeed += 0.9
      else 
        targetSpeed += 0.7
  console.log "target speed: " + targetSpeed
  #getDistanceToMaxSpeed
  distance = getDistanceTo position, nextCurveIndex, 0
  console.log "distance to next curve: " + distance
  timeToTravel = distance / state.speed
  console.log "timeToTravel: " + timeToTravel
  timeToDecelerate = getTimeToDecelerate state.speed, targetSpeed
  console.log "timeToDecelerate: " + timeToDecelerate
  if state.speed > targetSpeed and timeToTravel + 4 < timeToDecelerate or isCurve(position.piecePosition.pieceIndex) and state.speed > targetSpeed #for this curve!
  #getMinDistanceToSlowDownToTargetSpeed
  #brakingDistance = getBrakingDistance curSpeed, targetSpeed
  # currentspeed
  #if state.gameTick > 30
    return true
  else 
    return false
  # distanceToBrake

getNextSwitchPieceIndex = (pieceIndex) ->
  curPieceIndex = pieceIndex
  counter = race.trackNumPieces
  while counter > 0
    if curPieceIndex == race.trackNumPieces - 1
      curPieceIndex = 0
    else 
      curPieceIndex++
    if race.track.pieces[curPieceIndex].switch
      return curPieceIndex
    else
      counter--

getDirectionBetweenNextSwitches = (pieceIndex) ->
  nextSwitchIndex = getNextSwitchPieceIndex pieceIndex
  subsequentSwitchIndex = getNextSwitchPieceIndex nextSwitchIndex
  curPieceIndex = nextSwitchIndex

  turnCumulation = 0
  keepOn = true
  while keepOn
    if isCurve curPieceIndex
        turnCumulation += race.track.pieces[curPieceIndex].angle * race.track.pieces[curPieceIndex].radius
    if curPieceIndex == subsequentSwitchIndex - 1    
      keepOn = false
    if curPieceIndex == race.trackNumPieces - 1
      curPieceIndex = 0
    else 
      curPieceIndex++
  turnCumulation   

canSwitchToLeft = (curDistanceFromCenter) ->
  canSwitch = false
  for lane in race.track.lanes
    if lane.distanceFromCenter < curDistanceFromCenter
      canSwitch = true
  canSwitch

canSwitchToRight = (curDistanceFromCenter) ->
  canSwitch = false
  for lane in race.track.lanes
    if lane.distanceFromCenter > curDistanceFromCenter
      canSwitch = true
  canSwitch    

shouldTrySwitchLane = () ->
  laneChange = {}
  laneChange.goAhead = false
  laneChange.direction = "no"

  lane = state.position.piecePosition.lane.endLaneIndex
  
  direction = getDirectionBetweenNextSwitches state.position.piecePosition.pieceIndex
  if direction < 0
    if canSwitchToLeft race.track.lanes[lane].distanceFromCenter
      laneChange.goAhead = true
      laneChange.direction = "Left"
  else if direction > 0
    if canSwitchToRight race.track.lanes[lane].distanceFromCenter
      laneChange.goAhead = true
      laneChange.direction = "Right"
  # if direction == 0, straight ahead, no lane changes!
  laneChange
  
#assumes updateSituation has been run
nextMove = () ->
  move = {}
  move.type = null
  move.amount = 0

  laneChange = shouldTrySwitchLane()
  laneChange.goAhead = false #  TODO: set for competitions, lane switching before turns causes the bot to crash...
  if laneChange.goAhead and not mode.switchLanePending
    move.type = "switchLane"
    move.amount = laneChange.direction
    mode.switchLanePending = true
    mode.switchLanePendingDirection = laneChange.direction
  else
   if hasToBrake (state.position)
      console.log "braking"
      move.type = "brake"
    else
      move.type = "throttle"
      targetSpeed = 11
      if state.speed < (targetSpeed - 0.01)
        move.amount = 1.0
      else 
        move.amount = getThrottleForSpeed(targetSpeed)
  move